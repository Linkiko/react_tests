import React from 'react'
import PropTypes from 'prop-types'
import 'antd/dist/antd.css';
import { Table, Tag } from 'antd';

const TodoListCore = ({ todos, toggleTodo, remove }) => {
    const columns = [
        {
            title: 'Tâche',
            dataIndex: 'task',
            key: 'task',
        },
        {
            title: 'Etat',
            dataIndex: 'isDone',
            key: 'isDone',
            render: (isDone, todo) => {
                let color = isDone === true ? 'red' : 'green'
                let text = isDone === true ? 'Fait' : 'A Faire'
                return (
                    < span >
                        <Tag color={color} key={todo.id} onClick={() => toggleTodo(todo.key)}>
                            {text}
                        </Tag>
                    </span >
                )
            }
        },
        {
            title: 'Suppimer',
            dataIndex: 'delete',
            key: 'delete',
            render: (delete_action, todo) => {
                return (
                    <span>
                        <button onClick={() => remove(todo.key)}>Delete</button>
                    </span >
                )
            }
        }

    ]
    const array = []
    console.log(todos)
    for (let i = 0; i < todos.length; i++) {
        array.push({
            key: todos[i].id,
            task: todos[i].text,
            isDone: todos[i].completed
        })
    }
    const data = array.slice()

    return <Table columns={columns} dataSource={data} />

}

TodoListCore.propTypes = {
    todos: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            completed: PropTypes.bool.isRequired,
            text: PropTypes.string.isRequired
        }).isRequired
    ).isRequired,
    toggleTodo: PropTypes.func.isRequired
}

export default TodoListCore