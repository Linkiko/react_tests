import { connect } from 'react-redux'
import { setVisibilityFilter } from '../actions/todoListActions.js'
import Link from '../components/Link'

const mapStateToProperty = (state, ownProperty) => ({
    active: ownProperty.filter === state.visibilityFilter
})

const mapDispatchToProps = (dispatch, ownProperty) => {
    console.log(ownProperty)
    return ({
        onClick: () => dispatch(setVisibilityFilter(ownProperty.filter))
    })
}


export default connect(
    mapStateToProperty,
    mapDispatchToProps
)(Link)