import React from 'react';
import ReactDOM from 'react-dom';
import { createLogger } from 'redux-logger'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import rootReducer from './reducers'
import TodoList from './components/TodoList'

const loggerMidelleware = createLogger()

const store = createStore(rootReducer, applyMiddleware(loggerMidelleware))

class App extends React.Component {

    render() {
        return (
            <Provider store={store}>
                <TodoList />
            </Provider>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'));